//
//  ContentView.swift
//  Goo.fy
//
//  Created by Rodrigo on 2020/06/22.
//  Copyright © 2020 Rodrigo. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello World")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
