# iOS App for Video Creation And Sharing

Goo.fy is a short video creation and sharing platform that allows you to share some of your goofy videos. It allows you to social network with people whom you share the same interest with in terms of video content!

## Features

- Chat
- Post videos
- Add Audio/Song To videos
- Add Filters & Effects
- View User Profiles
- Follow/Unfollow
- Comment
- Like Videos
- Share Videos
- Animation Onboarding

## Splash and Onboarding

![alt text](screenshot/splash.PNG "splash screen")
![alt text](screenshot/onboard1.PNG "onboarding-1")
![alt text](screenshot/onboard2.PNG "onboarding-2")
![alt text](screenshot/onboard3.PNG "onboarding-3")

## ScreenShot

![alt text](screenshot/1.PNG "screenshot")
![alt text](screenshot/2.PNG "screenshot")
![alt text](screenshot/3.PNG "screenshot")
![alt text](screenshot/4.PNG "screenshot")
![alt text](screenshot/5.PNG "screenshot")
![alt text](screenshot/6.PNG "screenshot")
![alt text](screenshot/7.PNG "screenshot")
![alt text](screenshot/8.PNG "screenshot")
![alt text](screenshot/9.PNG "screenshot")
![alt text](screenshot/10.PNG "screenshot")
